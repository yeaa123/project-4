/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: Duration of the week
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	unsigned long countWords(const string& s, set<string>& wl);
	string line;
	string uniqueness;
	string word;
	set<string> s;
	char w;
	vector<string> v;
	int letters = 0;
	vector<string> wordcount;
	int x=0
	set<string> job;
	while(getline(cin,line)){
		if (line.empty()){
			letters++;
		}
		else line+="";

		s.insert(line); //s.size= unique linesonly
		v.push_back(line); //v.size= total linesonly
		for(int i=0; i< v[x].length(); i++){
			if( v[x][0]== '0'){
				v[x][0]='0';
			}
			char w =v[x][i];

			if(isspace(w) && v[x][i-1]=='0' || v[x][i]== '0' || v[x][i-1] == '0'){
				word.clear();
			}
			else if (isspace(w) && v[x][i-1]!='0'&& v[x][i-1]!='\t' && v[x][i-1]!='0'){
				job.insert(word);
				wordcount.push_back(word);
				word.clear();
			} else{
				word+=w;
			}
			letters++;
		}
		x++;  //this changes which vector value the for loop looks at

	}
	cout << v.size() << "\t" << wordcount.size() << "\t" << letters << "\t" << s.size() << "\t" << job.size() << "\n";

	return 0;
}
