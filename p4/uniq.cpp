#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

#include <vector>
using std::vector;
#include <sstream>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	string line;
	char word[100];
	istringstream get; //user input
	vector <char> letters;
	vector <string> words;
	unsigned long check = 0;
	std::getline(cin,line);
	for (int a = 0; a < line.length(); a++){
		check = 0;
		if (line[a] != ' '){
			for  (unsigned long b = 0;b < letters.size(); b++){
				if ( line[a] != letters[b]){
					check++;
				}
			}
			if ( check == letters.size()){
				letters.push_back(line[a]);
				showcount++;
			}
		}
	}
	cout << showcount;

	//Akash
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
			std::getline(cin,line);
			get.str(line);
			get.getline(word, 100, ' ');
			words.push_back(word);
			while(get.getline( word, 100, ' ')){
				dupsonly = 0;
				for ( unsigned long b = 0; b < words.size(); b++){
					if ( word == words[b]){
						dupsonly = 1;
					}
				}
				if (dupsonly == 0){
					words.push_back(word);
				}
				else {
					cout << word << " "; }
				}
			break;
//Yassmin
			case 'd':
			std::getline(cin,line);
			get.str (line) ;
			get.getline(word, 100, ' ');
			words.push_back(word);
			while ( get.getline ( word, 100, ' ')
			{
				words.push_back(word);
			}
			for ( unsigned long a = 0; a < words.size(); a++){
				check = 0;
				for ( unsigned long b = 0; b < words.size(); b++){
					if ( words[a] == words[b] ){
						check++;
					}
				}
				if (check == 1){
					cout << words[a] << " ";
				}
			}
			break;

			case 'u':
				return 0;
				break;

			case 'h':
				printf(usage,argv[0]);
				return 0;

			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */

	return 0;
}
